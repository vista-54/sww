

function User(params)
{
    this.id = -1;
    this.userId=-1;
    this.login = '';
    this.isLogged = false;
    
    var self = this;
    var __constructor = function() {
        fill(self, params);
    };
    
    
    __constructor();
}
function fill(object, params){
    if(params){
        //object = setToNullAllVariables(object);
        var msg = '';
        for(var currParam in params){
            //alert(currParam+"  "+params[currParam]);
            //alert('typeof self.'+currParam+': '+typeof self[currParam]);
            var camelCasedParam = toCamelCase(currParam);
            var currParamValue = params[currParam];
            
            // param is object like Week, Shift  and it is not null
            if(typeof currParamValue === 'object' & currParamValue !== null & !isArray(currParamValue)){
                var className = ''+currParam;
                
                if ( !( window[className] && typeof(window[className]) === 'function') ) { 
                    // function with name 'window[className]' not exists
                    msg+=('Object with name "'+className+'" not exists \n');
                    continue;
                }
                
                var currParamName = currParam[0].toLowerCase()+ currParam.slice(1);
                var nestedClass = new window[className]( params[currParam]);
                object[currParamName] = nestedClass;
                continue;
            }
            
            // param is array of some objects
            if(typeof currParamValue === 'object' & currParamValue !== null & isArray(currParamValue)){
                
                // deleting letter 's' example  Shifts => Shift
                var className = currParam.slice(0, -1);
                
                if ( !( window[className] && typeof(window[className]) === 'function') ) { 
                    // function with name 'window[className]' not exists
                    msg+=('Object with name "'+className+'" not exists \n');
                    continue;
                }
                
                var currParamValueArr = currParamValue;
                var nestedClassesArr = [];
                for(var i in currParamValueArr){
                    var nestedClass = new window[""+className]( currParamValueArr[i]);
                    nestedClassesArr.push(nestedClass);
                }
                var currParamName = currParam[0].toLowerCase()+ currParam.slice(1);
                object[currParamName] = nestedClassesArr;
                continue;
            }
            
            // param is null-type
            if(typeof currParamValue === 'object' & currParamValue === null){
                // if null-type is object(class)
                if( window[currParam] && typeof(window[currParam]) === 'function'){
                    var nestedClass = new window[""+currParam]();
                    var currParamName = currParam[0].toLowerCase()+ currParam.slice(1);
                    object[currParamName] = nestedClass;
                    
                }
                // if null-type is simple param
                else{
                    object[camelCasedParam] = null;
                }
                
            }
            
            
            if(typeof object[camelCasedParam] !=="undefined" && object[camelCasedParam] !==null){
           // if(!(camelCasedParam in object)){
            var type = typeof object[camelCasedParam];
                var currParamValueWithType = getVariableWithItRealType(type, currParamValue);
                object[camelCasedParam] = currParamValueWithType; //currParamValue;
            }else{
               // msg+=("variable  '"+camelCasedParam+"' not found in object '"+object.constructor.name+ "' ::  create it or change model ! \n");
            }
        }
        //msg += check(object);
        if(msg!==''){
            alert(msg);
            //log(msg);
        }
    }else{
        //object = null;
    }
}


function getVariableWithItRealType(type, value){
    
    if(type === 'number'){ return parseFloat(value); }
    return value;
}



function check(object){
    var objClassName = getObjectClass(object);
    var msg = '';
    if(object === null || typeof(object)==='undefined'){ 
        msg += 'object'+ objClassName +' is null';
    }else{
        for(var attr in object){
//            if(typeof(object[attr])==='undefined'){
//                msg += objClassName+'['+ attr +'] is undefined \n';
//            }
            if(object[attr] === null){
                msg += objClassName+'['+ attr +'] is null or undefined \n';
            }
        }
    }
    return msg;
}

function getObjectClass(obj){
   if (typeof obj !== "object" || obj === null) return false;
   else return /(\w+)\(/.exec(obj.constructor.toString())[1];
}

function toCamelCase(input) {
    return input./*toLowerCase(). */replace(/_([A-z])/g, function(match, group1) {
        return group1.toUpperCase();
    });
}

function setToNullAllVariables(object){
    for(var attr in object){
        var isDate = Object.prototype.toString.call(object[attr]) === '[object Date]';
        if(isArray(object[attr]) ){ continue; }
        if(typeof object[attr] === 'object' & !isDate ){
                continue;
        }
        if(typeof object[attr] === 'function' ){
                continue;
        }
        object[attr] = null;
    }
    return object;
}
